<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'userlist'], function(){
    Route::get('fetch','UserListController@fetch');
    Route::post('post','UserListController@post');
    Route::post('login','authController@login');
    Route::post('delete','UserListController@delete');
});
Route::post('login','authController@login');
Route::post('register','authController@register');
