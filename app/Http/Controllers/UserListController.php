<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\userlist;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserListController extends Controller
{
    function fetch(){
        $userlist = userlist::get();
        return response()->json($userlist, 200);
    }
    function post(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email'
            ]);
            
            $user = new userlist;
            $user->name = $request->input('name');
            $user->address = $request->input('address');
            $user->email = $request->input('email');
            $user->password = $request->input('password');   
            $user->save();

            $token = JWTAuth::fromUser($user);
            DB::commit();
            return response()->json(["message"=>"success", 'token' =>$token], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>$e->getMessage()], 500);
        }   
    }
    function delete(Request $request){
        DB::beginTransaction();
        try{
            userlist::get()->where('id','=',$request->input('id'))->first()->delete();
            
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>$e->getMessage()], 500);
        }    
    }
}
